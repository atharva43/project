package com.example.TransformAPI;
import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;


@RestController
public class Controller {
    @RequestMapping(value = "edp/transformQuery",method = RequestMethod.POST)
    public JSONObject GsonData(@RequestBody String json)
    {
        Gson gson = new Gson();
        Input input = gson.fromJson(json, Input.class);

        Wrapper wrapper = new Wrapper(input);
//          JSONObject Jsonobj = new JSONObject();

        String Jsonobj = wrapper.TransformQuery();
        System.out.println(Jsonobj);
        return null;
    }
}
